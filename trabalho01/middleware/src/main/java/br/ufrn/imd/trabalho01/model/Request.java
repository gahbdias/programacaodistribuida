package br.ufrn.imd.trabalho01.model;

import java.io.Serializable;

public class Request implements Serializable {

    private String serverName;
    private String clientName;
    private String secretMsg;
    private String clienteKey;

    public Request(String serverName, String clientName, String secretMsg, String clienteKey){
        this.clientName = clientName;
        this.serverName = serverName;
        this.secretMsg = secretMsg;
        this.clienteKey = clienteKey;
    }

    public String getServerName() {
        return this.serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getSecretMsg() {
        return this.secretMsg;
    }

    public void setSecretMsg(String secretMsg) {
        this.secretMsg = secretMsg;
    }

    public String getClienteKey() {
        return this.clienteKey;
    }

    public void setClienteKey(String clienteKey) {
        this.clienteKey = clienteKey;
    }

    public String getClientName() {
        return this.clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

}
