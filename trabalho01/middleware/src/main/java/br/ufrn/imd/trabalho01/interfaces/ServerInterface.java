package br.ufrn.imd.trabalho01.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerInterface extends Remote{

    void registerMiddleware(MiddlewareInterface middleware) throws RemoteException;
    MiddlewareInterface getMiddleware(MiddlewareInterface middleware) throws RemoteException;
    
}
