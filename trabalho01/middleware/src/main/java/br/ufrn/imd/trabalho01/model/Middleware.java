package br.ufrn.imd.trabalho01.model;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Iterator;

import br.ufrn.imd.trabalho01.interfaces.ClientInterface;
import br.ufrn.imd.trabalho01.interfaces.ServerInterface;
import br.ufrn.imd.trabalho01.model.mapping.ResponseType;
import br.ufrn.imd.trabalho01.model.mapping.Transaction;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Middleware extends MiddlewareAbstract {

	public Middleware(String serverName) throws RemoteException {
		super(serverName);
		ServerInterface lookupServer;
		try {
			lookupServer = (ServerInterface) Naming.lookup("rmi://127.0.0.1:1099/Mastercard");
			lookupServer.registerMiddleware(this);
			// lookupServer = (ServerInterface) Naming.lookup("rmi://127.0.0.1:1020/Visa");
			// lookupServer.registerMiddleware(this);
			// lookupServer = (ServerInterface) Naming.lookup("rmi://127.0.0.1:1021/PayPal");
			// lookupServer.registerMiddleware(this);
		} catch (MalformedURLException | NotBoundException e) {
			log.error(e.getLocalizedMessage());
		}	
		new Notify().start();
	}

    private class Notify extends Thread{
		
        @Override
		public void run() {
			
			while(true) {
				if(!clients.isEmpty()) {
					log.info("Notificando clientes");
					Iterator<ClientInterface> it = clients.values().iterator();
					while (it.hasNext()) {
						ClientInterface client = it.next();
						Transaction transaction = Transaction.BEGIN;
						Response res = new Response("", ResponseType.OK);
						try {
							Request req = client.getRequest();
							res.setClientIdentifier(req.getClienteKey());
							if(!req.getSecretMsg().isEmpty()){
								String[] data = req.getSecretMsg().split("#");
								String cardN = data[0];
								transaction = Transaction.valueOf(data[1]);
								String value = data[2];
								log.info("Client "+req.getClientName()+" with card from "+req.getServerName()+": ("+cardN+") want to "+transaction+" "+value);
							}else {
								log.info("- Nada para notificar");
							}
							client.setResponse(res);
						} catch (RemoteException e) {
							it.remove();
						}
						if(transaction.equals(Transaction.END)){
							res.setResult(ResponseType.END);
							it.remove();
						}
					}

					try {
						int nClients = clients.size();
						int nResponses = responses.size();
						it = clients.values().iterator();
						Iterator<Response> rIterator = responses.iterator();
						while(rIterator.hasNext() && it.hasNext()) {
							Response resp = rIterator.next();
							ClientInterface c = it.next();
							if(c.getRequest().getClienteKey().equals(resp.getClientIdentifier())) {
								Request req = c.getRequest();
								Response res = c.getResponse();
								req.setSecretMsg("");
								res.setResult(processResponse(resp.getResult()));
								c.setRequest(req);
								c.setResponse(res);
							}else {
								if(nResponses > nClients){
									while(!c.getRequest().getClienteKey().equals(resp.getClientIdentifier()) && rIterator.hasNext()){
										nResponses--;
										resp = rIterator.next();
									}
								}else {
									while(!c.getRequest().getClienteKey().equals(resp.getClientIdentifier()) && it.hasNext()){
										nClients--;
										c = it.next();
									}
								}
							}
							rIterator.remove();
						}
					} catch (RemoteException e) {
						// Cliente removido acima ou deixar cod acima resolver na proxima iteração
					}
					
				} else{
					log.info("Sem clientes para notificar");
				}

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
                    Thread.currentThread().interrupt();
				}
			}
		}

		private ResponseType processResponse(ResponseType result) {
			ResponseType finalResult = result;
			if(result.equals(ResponseType.INVALID) || result.equals(ResponseType.FAILED) ){
				finalResult = ResponseType.END;
			}
			return finalResult;
		}
	}
}
