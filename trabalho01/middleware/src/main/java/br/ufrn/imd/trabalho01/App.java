package br.ufrn.imd.trabalho01;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import br.ufrn.imd.trabalho01.interfaces.MiddlewareInterface;
import br.ufrn.imd.trabalho01.model.Middleware;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class App {
    public static void main( String[] args )
    {
        System.setProperty("java.rmi.server.hostname","127.0.0.1");
		MiddlewareInterface server;
        final String serverName = "TransactionServer";
		try {
            server = new Middleware(serverName);
            LocateRegistry.createRegistry(1098);
            Naming.rebind("rmi://127.0.0.1:1098/"+serverName, server);
        } catch (RemoteException | MalformedURLException e) {
            log.error(e.getLocalizedMessage());
        }
		log.info("RMI Callback Middleware Starterd.");
    }
}
