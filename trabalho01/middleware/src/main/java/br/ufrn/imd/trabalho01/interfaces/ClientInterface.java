package br.ufrn.imd.trabalho01.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import br.ufrn.imd.trabalho01.model.Request;
import br.ufrn.imd.trabalho01.model.Response;

public interface ClientInterface extends Remote {

    void setResponse(Response response) throws RemoteException;
    void setRequest(Request request) throws RemoteException;
    
    Request getRequest() throws RemoteException;
    Response getResponse() throws RemoteException;
    
}
