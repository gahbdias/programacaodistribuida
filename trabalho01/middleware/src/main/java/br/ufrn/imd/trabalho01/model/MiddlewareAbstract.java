package br.ufrn.imd.trabalho01.model;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import br.ufrn.imd.trabalho01.interfaces.ClientInterface;
import br.ufrn.imd.trabalho01.interfaces.MiddlewareInterface;
import br.ufrn.imd.trabalho01.model.mapping.ResponseType;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class MiddlewareAbstract extends UnicastRemoteObject implements MiddlewareInterface {

	protected transient ConcurrentHashMap<String,ClientInterface> clients = new ConcurrentHashMap<>();
	protected transient List<Request> requests = new ArrayList<>();
	protected transient List<Response> responses = new ArrayList<>();
	protected String identifier;

    protected MiddlewareAbstract(String serverName) throws RemoteException {
		super();
		this.identifier = serverName;
	}

	@Override
    public void registerClient(ClientInterface client) throws RemoteException {
		String clientKey = client.getRequest().getClienteKey();
        clients.putIfAbsent(clientKey, client);
		clients.get(clientKey).setResponse(new Response(clientKey, ResponseType.OK));
		log.info("Novo cliente registrado com sucesso! Total: "+clients.size());
    }

	@Override
	public ClientInterface getClient(ClientInterface client) throws RemoteException {
		return clients.get(client.getRequest().getClienteKey());
	}

	@Override
	public void setResponse(List<Response> response) throws RemoteException {
		this.responses = response;
	}

	@Override
	public void setRequest(List<Request> request) throws RemoteException {
		this.requests = request;
	}

	@Override
	public List<Request> getRequest() throws RemoteException {
		final List<Request> reqs = new ArrayList<>();
		clients.values().forEach(client -> {
			try {
				reqs.add(client.getRequest());
			} catch (RemoteException e) {
				log.error(e.getLocalizedMessage());
			}
		});
		return reqs;
	}

	@Override
	public List<Response> getResponse() throws RemoteException {
		final List<Response> resp = new ArrayList<>();
		clients.values().forEach(client -> {
			try {
				resp.add(client.getResponse());
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		});
		return resp;
	}

	@Override
	public String getIdentifier() throws RemoteException {
		return this.identifier;
	}
}
