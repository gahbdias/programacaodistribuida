package br.ufrn.imd.trabalho01.model.mapping;

public enum ResponseType {
    OK, VALID, INVALID, COMPLETED, FAILED, END;
}
