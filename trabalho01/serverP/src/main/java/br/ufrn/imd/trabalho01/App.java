package br.ufrn.imd.trabalho01;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import br.ufrn.imd.trabalho01.interfaces.ServerInterface;
import br.ufrn.imd.trabalho01.model.Server;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class App {
    public static void main( String[] args )
    {
        System.setProperty("java.rmi.server.hostname","127.0.0.1");
		ServerInterface server;
        final int port = 1021;
        final String flag = "PayPal";
		try {
            server = new Server(flag);
            LocateRegistry.createRegistry(port);
            Naming.rebind("rmi://127.0.0.1:"+port+"/"+flag, server);
        } catch (RemoteException | MalformedURLException e) {
            e.printStackTrace();
        }
		log.info("RMI Callback Server Starterd.");
    }
}
