package br.ufrn.imd.trabalho01.model;

public class Client {

    private String nome;
    private String cardNumber;
    private double credito;


    public Client(String nome, String cardNumber, double credito) {
        this.nome = nome;
        this.cardNumber = cardNumber;
        this.credito = credito;
    }

    public double getCredito() {
        return this.credito;
    }

    public void setCredito(double credito) {
        this.credito = credito;
    }

    public String getCardNumber() {
        return this.cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
}
