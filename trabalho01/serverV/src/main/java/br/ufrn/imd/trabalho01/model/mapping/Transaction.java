package br.ufrn.imd.trabalho01.model.mapping;

public enum Transaction {
    BEGIN, VALIDATE, BUY, END;
}
