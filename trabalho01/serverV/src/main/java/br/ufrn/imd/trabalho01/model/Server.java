package br.ufrn.imd.trabalho01.model;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import br.ufrn.imd.trabalho01.interfaces.MiddlewareInterface;
import br.ufrn.imd.trabalho01.model.mapping.ResponseType;
import br.ufrn.imd.trabalho01.model.mapping.Transaction;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class Server extends ServerAbstract {

	private ConcurrentHashMap<String, Client> clients = new ConcurrentHashMap<>();

	public Server(String serverName) throws RemoteException {
		super(serverName);
		clients.putIfAbsent("123456", new Client("Paulo","123456",50));
		clients.putIfAbsent("654321", new Client("Gabriela","654321",100));
		new Notify().start();
	}
    private class Notify extends Thread{
		
        @Override
		public void run() {
			
			while(true) {

				if(!middlewares.isEmpty()) {
					log.info("Notificando Middlewares");
					Iterator<MiddlewareInterface> it = middlewares.values().iterator();

					while (it.hasNext()) {
						MiddlewareInterface middleware = it.next();
						List<Request> requests = new ArrayList<>();
						List<Response> responses = new ArrayList<>();
						try {
							requests = middleware.getRequest();
							for (Request req : requests) {
								responses.add(processClientRequest(req));
							}
							middleware.setResponse(responses);
						} catch (RemoteException e) {
							it.remove();
						}
					}
				} else{
					log.info("Sem middlewares para notificar");
				}

				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
                    Thread.currentThread().interrupt();
				}
			}
		}

		private Response processClientRequest(Request req) {
			String cardN = "";
			double value;
			Transaction transaction;
			Response res = new Response(req.getClienteKey(), ResponseType.OK);

			if(!req.getSecretMsg().isEmpty()){
				String[] data = req.getSecretMsg().split("#");
				cardN = data[0];
				transaction = Transaction.valueOf(data[1]);
				value = Double.parseDouble(data[2]);

				if(transaction.equals(Transaction.VALIDATE)){
					if(clients.get(cardN).getCredito() > 0.0) res.setResult(ResponseType.VALID);
					else res.setResult(ResponseType.INVALID);
				}else if(transaction.equals(Transaction.BUY)){
					double credito = clients.get(cardN).getCredito();
					if( credito < value) res.setResult(ResponseType.FAILED);
					else{
						clients.get(cardN).setCredito(credito-value);
						res.setResult(ResponseType.COMPLETED);
					}
				}else{
					res.setResult(ResponseType.END);
				}
			}
			return res;
		}
	}
}
