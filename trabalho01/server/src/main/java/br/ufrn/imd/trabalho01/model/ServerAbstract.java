package br.ufrn.imd.trabalho01.model;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.ConcurrentHashMap;

import br.ufrn.imd.trabalho01.interfaces.MiddlewareInterface;
import br.ufrn.imd.trabalho01.interfaces.ServerInterface;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class ServerAbstract extends UnicastRemoteObject implements ServerInterface {

	protected transient ConcurrentHashMap<String,MiddlewareInterface> middlewares = new ConcurrentHashMap<>();
	protected String identifier;

    protected ServerAbstract(String serverName) throws RemoteException {
		super();
		this.identifier = serverName;
	}

	@Override
    public void registerMiddleware(MiddlewareInterface middleware) throws RemoteException {
        middlewares.putIfAbsent(middleware.getIdentifier(), middleware);
		log.info("Novo middleware registrado com sucesso! Total: "+middlewares.size());
    }

	@Override
	public MiddlewareInterface getMiddleware(MiddlewareInterface middleware) throws RemoteException {
		return middlewares.get(middleware.getIdentifier());
	}
}
