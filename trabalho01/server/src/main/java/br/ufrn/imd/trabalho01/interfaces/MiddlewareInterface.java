package br.ufrn.imd.trabalho01.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import br.ufrn.imd.trabalho01.model.Request;
import br.ufrn.imd.trabalho01.model.Response;

public interface MiddlewareInterface extends Remote {

    void setResponse(List<Response> response) throws RemoteException;
    void setRequest(List<Request> request) throws RemoteException;
    
    List<Request> getRequest() throws RemoteException;
    List<Response> getResponse() throws RemoteException;

    String getIdentifier() throws RemoteException;
    
}
