package br.ufrn.imd.trabalho01;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import br.ufrn.imd.trabalho01.interfaces.ClientInterface;
import br.ufrn.imd.trabalho01.interfaces.MiddlewareInterface;
import br.ufrn.imd.trabalho01.model.Client;
import br.ufrn.imd.trabalho01.model.Request;
import br.ufrn.imd.trabalho01.model.mapping.ResponseType;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class App {
    public static void main( String[] args )
    {
        Menu menu = new Menu();
        MiddlewareInterface server;
        try {
            server = (MiddlewareInterface) Naming.lookup("rmi://127.0.0.1:1098/TransactionServer");
            ClientInterface client = new Client("Paulo", "lphj1Di0dKuz");
            // ClientInterface client = new Client("Gabriela", "qq8jOWn220fg");
		    server.registerClient(client);
            Request request;
            while(true){
                ClientInterface c = server.getClient(client);
                if(c == null || c.getResponse().getResult().equals(ResponseType.END)){
                    Runtime.getRuntime().exit(0);
                    return;
                }
                request = menu.beginInteraction(client.getRequest().getClienteKey(), client.getRequest().getClientName());
                c.setRequest(request);
                Thread.sleep(5000);
            }
        } catch (MalformedURLException | RemoteException | NotBoundException | InterruptedException e) {
            log.error(e.getLocalizedMessage());
            Thread.currentThread().interrupt();
        }
    }
}
