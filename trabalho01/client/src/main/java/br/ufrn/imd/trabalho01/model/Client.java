package br.ufrn.imd.trabalho01.model;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import br.ufrn.imd.trabalho01.interfaces.ClientInterface;

public class Client extends UnicastRemoteObject implements ClientInterface {

    private Request request;
    private Response response;
    private String name;
    private String clientKey;

    public Client(String name, String clientKey) throws RemoteException {
      this.request = new Request("", name, "", clientKey);
      this.response = null;
      this.name = name;
      this.clientKey = clientKey;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClientKey() {
        return this.clientKey;
    }

    public void setClientKey(String clientKey) {
        this.clientKey = clientKey;
    }

    @Override
    public void setResponse(Response response) throws RemoteException {
        this.response = response;
    }

    @Override
    public void setRequest(Request request) throws RemoteException {
        this.request = request;
    }

    @Override
    public Request getRequest() throws RemoteException {
        return this.request;
    }

    @Override
    public Response getResponse() throws RemoteException {
        return this.response;
    }
}
