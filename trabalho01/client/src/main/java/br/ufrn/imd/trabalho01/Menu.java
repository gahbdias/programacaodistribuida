package br.ufrn.imd.trabalho01;

import java.util.Scanner;

import br.ufrn.imd.trabalho01.model.Request;
import br.ufrn.imd.trabalho01.model.mapping.Transaction;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Menu {

    public Menu(){ /* Apenas para nao usar metodos estaticos */ }

    public Request beginInteraction(String clientKey, String clientName) {
        Request request = null;
        final String DEFAULT_FLAG = "Escolha uma bandeira";
        String bandeira = DEFAULT_FLAG;
        String dados = "";
        int op = -1;
        while(op < 0){
            log.info("Menu:("+bandeira+")\n1 - Validar\n2 - Comprar\n3 - Bandeira\n0 - Finalizar transação\n");
            Scanner input = new Scanner(System.console().reader());
            op = input.nextInt();
            input.close();
            switch (op) {
                case 1:
                    request = new Request(bandeira, clientName, dados+"#"+Transaction.VALIDATE.name()+"#0", clientKey);
                    break;
                case 2:
                    request = new Request(bandeira, clientName, dados+"#"+Transaction.BUY.name()+"#500", clientKey);
                    break;
                case 3:
                    dados = preencherBandeira();
                    if(dados != null){
                        String[] tmp = dados.split("#");
                        bandeira = tmp[0];
                        if(bandeira == null) bandeira = DEFAULT_FLAG;
                        dados = tmp[1];
                    } else{
                        dados = "";
                    }
                    break;
                case 0:
                    request = new Request("", clientName, "0#"+Transaction.END.name()+"#0", clientKey);
                    break;
                default:
                    op = -1;
                    break;
            }

            if(op > 0 && (dados.equals("") || request == null)){
                op = -1;
            }
            
        }
        return request;
    }

    private String preencherBandeira(){
        String dados = "";
        log.info("Menu:\n1 - MasterCard\n2 - Visa\n3 - PayPal\n0 - Voltar\n");
        Scanner input = new Scanner(System.console().reader());
        int bandeira = input.nextInt();
        switch (bandeira) {
            case 1:
                dados = "MasterCard#";
                dados+=preencherDados();
                break;
            case 2:
                dados = "Visa#";
                dados+=preencherDados();
                break;
            case 3:
                dados = "PayPal#";
                dados+=preencherDados();
                break;
            default:
                break;
        }
        input.close();
        return dados;
    }

    private String preencherDados(){
        String dados = "";
        log.info("Dados do cartao: ");
        Scanner input = new Scanner(System.console().reader());
        dados = input.nextLine();
        input.close();
        return (dados == null ? "" : dados);
    }

    
}
