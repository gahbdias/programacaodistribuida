# Programacaodistribuida

Implemente um servidor de transações que dê suporte a semântica de chamadas
remotas. Tal servidor deve:
a. Receber solicitações de clientes
b. Encaminhar as solicitações para servidores “hipotéticos”
c. Manter o registro das solicita

## Relatório
https://docs.google.com/document/d/1A2je3fPABV3BPsPjkCyIZmVjqtHSJGc2/edit?usp=sharing&ouid=113143295927153964414&rtpof=true&sd=true
