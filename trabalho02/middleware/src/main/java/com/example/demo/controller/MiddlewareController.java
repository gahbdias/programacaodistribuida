package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exceptions.RestRequestException;
import com.example.demo.logic.MiddlewareLogic;
import com.example.demo.model.Pair;
import com.example.demo.model.Request;
import com.example.demo.model.Response;
import com.example.demo.model.mapping.ResponseType;
import com.fasterxml.jackson.core.JsonProcessingException;



@RestController
public class MiddlewareController {

    @Autowired
    MiddlewareLogic middleLogic;

    @PostMapping(value = "/process")
    public Response findById(@RequestBody Request request){
        Pair pair;
        if( !middleLogic.getClients().containsKey(request.getClienteKey())){
            pair = new Pair();
            middleLogic.getClients().putIfAbsent(request.getClienteKey(), pair);
        }else{
            pair = middleLogic.getClients().get(request.getClienteKey());
        }
        pair.add(request, null);
        Response response = new Response(request.getClienteKey(), ResponseType.END);
        try {
            response = middleLogic.postToServer(request);
        } catch (RestRequestException | JsonProcessingException e) {
            e.printStackTrace();
        }

        pair.updateResponse(response);
        middleLogic.getClients().replace(request.getClienteKey(), pair);
        
        if(response.getResult().equals(ResponseType.END)){
            pair.log();
        }

        return response;
    }

}