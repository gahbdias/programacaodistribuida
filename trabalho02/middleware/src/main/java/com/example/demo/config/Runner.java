package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.logic.MiddlewareLogic;

@Configuration
public class Runner {
    @Bean
    public MiddlewareLogic serverInit() {
        return new MiddlewareLogic();
    }
}
