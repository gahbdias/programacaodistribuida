package com.example.demo.model;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Pair {

    private List<Request> requests = new ArrayList<>();
    private List<Response> resposes = new ArrayList<>();

    public Pair() {
        //NTD
    }

    public void add(Request req, Response res){
        this.requests.add(req);
        this.resposes.add(res);
    }

    public void updateRequest(Request req){
        this.requests.add(this.index(), req);
    }
    
    public void updateResponse(Response res){
        this.resposes.add(this.index(), res);
    }

    public void updateRequest(int index, Request req){
        this.requests.add(index, req);
    }
    
    public void updateResponse(int index, Response res){
        this.resposes.add(index, res);
    }
    
    public int index(){
        return this.requests.size() - 1;
    }

    public int size(){
        return this.requests.size();
    }

    public void log() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < this.requests.size(); i++) {
            stringBuilder.append("\nRequest:"+i+"\n");
            stringBuilder.append(this.requests.get(i).toJson());
            stringBuilder.append("\nResponse:"+i+"\n");
            if(this.resposes.get(i) == null) stringBuilder.append("{}");
            else stringBuilder.append(this.resposes.get(i).toJson());
        }
        log.info(stringBuilder.toString());
    }
}
