package com.example.demo.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.example.demo.model.mapping.ResponseType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class Response implements Serializable {
    private String clientIdentifier;
    private ResponseType result;
    public static final ObjectMapper mapper = new ObjectMapper();

    public Response(String clientIdentifier, ResponseType result){
        this.clientIdentifier = clientIdentifier;
        this.result = result;
    }

    public void setClientIdentifier(String clientIdentifier) {
        this.clientIdentifier = clientIdentifier;
    }

    public String getClientIdentifier() {
        return this.clientIdentifier;
    }

    public ResponseType getResult() {
        return this.result;
    }

    public void setResult(ResponseType result) {
        this.result = result;
    }

    public String toJson(){
        return "{"+
            "\n\"clientIdentifier\""+":"+"\""+this.getClientIdentifier()+ "\"," +
            "\n\"result\""+":"+"\""+this.getResult()+ "\"" +
            "\n}";
    }

    public static Response fromJson(String response) throws JsonProcessingException {
        Map<String, String> jsonMap = mapper.readValue(response, HashMap.class);
		return new Response(jsonMap.get("clientIdentifier"), ResponseType.valueOf(jsonMap.get("result")));
    }
}
