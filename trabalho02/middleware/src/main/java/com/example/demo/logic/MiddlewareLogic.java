package com.example.demo.logic;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.example.demo.exceptions.RestRequestException;
import com.example.demo.model.HttpUtils;
import com.example.demo.model.Pair;
import com.example.demo.model.Request;
import com.example.demo.model.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class MiddlewareLogic {

	private ConcurrentHashMap<String, Pair> clients = new ConcurrentHashMap<>();
	private Map<String, String> servers = new HashMap<>();

	public MiddlewareLogic(){
		servers.put("MasterCard","http://localhost:8090/process");
		servers.put("Visa","http://localhost:8091/process");
		servers.put("PayPal","http://localhost:8092/process");
	}
    
	public ConcurrentMap<String, Pair> getClients() {
		return clients;
	}
    
	public Response postToServer(Request request) throws RestRequestException, JsonProcessingException{
		String uri = servers.getOrDefault(request.getServerName(), "http://localhost:8090/process");
		Map<String, String> headerParams = new HashMap<>();

		headerParams.put("accept", "application/json");
		headerParams.put("content-type", "application/json");
		String req = request.toJson();
		String response = HttpUtils.httpPostRequest(uri, headerParams, req, 200);
		return Response.fromJson(response);
	}
}
