package com.example.demo.model;

import java.io.Serializable;

import com.example.demo.model.mapping.ResponseType;


public class Response implements Serializable {
    private String clientIdentifier;
    private ResponseType result;

    public Response(String clientIdentifier, ResponseType result){
        this.clientIdentifier = clientIdentifier;
        this.result = result;
    }

    public void setClientIdentifier(String clientIdentifier) {
        this.clientIdentifier = clientIdentifier;
    }

    public String getClientIdentifier() {
        return this.clientIdentifier;
    }

    public ResponseType getResult() {
        return this.result;
    }

    public void setResult(ResponseType result) {
        this.result = result;
    }
}
