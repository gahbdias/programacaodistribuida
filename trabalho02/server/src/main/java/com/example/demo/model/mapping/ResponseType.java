package com.example.demo.model.mapping;

public enum ResponseType {
    OK, VALID, INVALID, COMPLETED, FAILED, END;
}
