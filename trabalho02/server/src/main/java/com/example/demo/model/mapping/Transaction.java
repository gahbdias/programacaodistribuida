package com.example.demo.model.mapping;

public enum Transaction {
    BEGIN, VALIDATE, BUY, END;
}
