package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.logic.ServerLogic;
import com.example.demo.model.Request;
import com.example.demo.model.Response;



@RestController
public class ServerController {

    @Autowired
    ServerLogic serverLogic;

    @PostMapping(value = "/process")
    public Response findById(@RequestBody Request request){
        return serverLogic.processClientRequest(request);
    }

}