package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.logic.ServerLogic;

@Configuration
public class Runner {
    @Bean
    public ServerLogic serverInit() {
        return new ServerLogic();
    }
}
