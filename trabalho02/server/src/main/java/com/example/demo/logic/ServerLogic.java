package com.example.demo.logic;

import java.util.concurrent.ConcurrentHashMap;

import com.example.demo.model.Client;
import com.example.demo.model.Request;
import com.example.demo.model.Response;
import com.example.demo.model.mapping.ResponseType;
import com.example.demo.model.mapping.Transaction;

public class ServerLogic {

    private ConcurrentHashMap<String, Client> clients = new ConcurrentHashMap<>();

	public ServerLogic(){
		clients.putIfAbsent("123456", new Client("Paulo","123456",500));
		clients.putIfAbsent("654321", new Client("Gabriela","654321",1000));
	}
    
    public Response processClientRequest(Request req) {
        String cardN = "";
        double value;
        Transaction transaction;
        Response res = new Response(req.getClienteKey(), ResponseType.OK);

        if(!req.getSecretMsg().isEmpty()){
            String[] data = req.getSecretMsg().split("#");
            cardN = data[0];
            transaction = Transaction.valueOf(data[1]);
            value = Double.parseDouble(data[2]);

            if(transaction.equals(Transaction.VALIDATE)){
                if(clients.get(cardN).getCredito() > 0.0) res.setResult(ResponseType.VALID);
                else res.setResult(ResponseType.INVALID);
            }else if(transaction.equals(Transaction.BUY)){
                double credito = clients.get(cardN).getCredito();
                if( credito < value) res.setResult(ResponseType.FAILED);
                else{
                    clients.get(cardN).setCredito(credito-value);
                    res.setResult(ResponseType.COMPLETED);
                }
            }else{
                res.setResult(ResponseType.END);
            }
        }
        return res;
    }
}
