package br.ufrn.imd.trabalho01.model;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.ufrn.imd.trabalho01.exceptions.RestRequestException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Client {

    private String name;
    private String clientKey;

    public Client(String name, String clientKey){
      this.name = name;
      this.clientKey = clientKey;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClientKey() {
        return this.clientKey;
    }

    public void setClientKey(String clientKey) {
        this.clientKey = clientKey;
    }

    public void post(Request request) throws RestRequestException, JsonProcessingException {
		String uri = "http://localhost:8085/process";
		Map<String, String> headerParams = new HashMap<String, String>();

		headerParams.put("accept", "application/json");
		headerParams.put("content-type", "application/json");
		String response = HttpUtils.httpPostRequest(uri, headerParams, request.toJson(), 200);
        Response res = Response.fromJson(response);
		log.info("Http Response has "+res.getResult()+".\n");
	}
    
}
