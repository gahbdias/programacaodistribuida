package br.ufrn.imd.trabalho01;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.ufrn.imd.trabalho01.exceptions.RestRequestException;
import br.ufrn.imd.trabalho01.interfaces.ClientInterface;
import br.ufrn.imd.trabalho01.interfaces.MiddlewareInterface;
import br.ufrn.imd.trabalho01.model.Client;
import br.ufrn.imd.trabalho01.model.Request;
import br.ufrn.imd.trabalho01.model.mapping.ResponseType;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class App {
    public static void main( String[] args )
    {
        Menu menu = new Menu();
        // ClientInterface client = new Client("Paulo", "lphj1Di0dKuz");
        // ClientInterface client = new Client("Gabriela", "qq8jOWn220fg");
        Client client = new Client("Paulo", "lphj1Di0dKuz");

        Request request;
        while(true){
            request = menu.beginInteraction(client.getClientKey(), client.getName());
            if(request == null) break;
            try {
                client.post(request);
            } catch (RestRequestException | JsonProcessingException e) {
                log.info(e.getMessage());
            }
        }
    }
}
